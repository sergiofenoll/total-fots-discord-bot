import config
import logging
import discord
import io
from discord.ext import commands

# logger = logging.getLogger("discord")
# logger.setLevel(logging.DEBUG)
# handler = logging.FileHandler(filename="tofots.log", encoding="utf-8", mode="w")
# handler.setFormatter(
#     logging.Formatter("%(asctime)s:%(levelname)s:%(name)s: %(message)s")
# )
# logger.addHandler(handler)

description = """
Moderation bot for the Total FotS Discord server.
"""
bot = commands.Bot(
    command_prefix=commands.when_mentioned_or('!'), description=description, pm_help=None
)

@bot.event
async def on_member_join(member):
    await member.add_roles(member.guild.get_role(config.member_role_id))
    async for rule_message in bot.get_channel(config.rules_channel_id).history(oldest_first=True, limit=None):
        files = [discord.File(io.BytesIO(await atch.read()), filename=atch.filename) for atch in rule_message.attachments] or None
        embed = rule_message.embeds or None
        await member.send(rule_message.content, files=files, embed=embed)


@bot.event
async def on_command_error(ctx, error):
    if isinstance(error, commands.NoPrivateMessage):
        await ctx.send(
            f"Hiya {ctx.author.display_name}! The command you used is not supported via DMs. Try using it in the "
            f"server instead."
        )

@bot.command(help="Prompts the bot to send you the rules via DM.")
@commands.guild_only()
async def rules(ctx):
    await ctx.message.delete()
    await ctx.author.send(
        f"Hi {ctx.author.display_name}! You can find the rules of the **{ctx.guild.name}** server below, "
        f"as well as in the <#{config.rules_channel_id}> channel."
    )
    async for rule_message in ctx.guild.get_channel(config.rules_channel_id).history(oldest_first=True, limit=None):       
        files = [discord.File(io.BytesIO(await atch.read()), filename=atch.filename) for atch in rule_message.attachments] or None
        embed = rule_message.embeds or None
        await ctx.author.send(rule_message.content, files=files, embed=embed)


@bot.command(help="Prompts the bot send you some server and mod related info via DM.")
@commands.guild_only()
async def info(ctx):
    await ctx.message.delete()
    await ctx.author.send(
        f"Hi {ctx.author.display_name}! You can find some info about the **{ctx.guild.name}** server and the mod it's "
        f"about below, as well as in the <#{config.info_channel_id}> channel."
    )
    async for info_message in (ctx.guild.get_channel(config.info_channel_id).history(oldest_first=True, limit=None)):
        files = [discord.File(io.BytesIO(await atch.read()), filename=atch.filename) for atch in info_message.attachments] or None
        embed = info_message.embeds or None
        await ctx.author.send(info_message.content, files=files, embed=embed)


if __name__ == "__main__":
    bot.run(config.token)
